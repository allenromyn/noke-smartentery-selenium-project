package com.smartentry.automation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

public class CompanyManagerChangeHoursofOperationTests extends SMSmartEntryBaseClassEmail  {

	@Test
	public void companyManagerChangeHoursofOperationTest() {

		// reuseable variables
		String op = "6"; // am
		String cl = "7"; // pm
		String min = "00";
		
		
		
		System.out.println("Starting CompanyManagerChangeHoursofOperationTests test.");

		// Login Setup 
		WebDriver driver = loginSetup();
		sleep(5000);
		
	
		// slow window
		// select settings button
		System.out.println("Select settings link.");
		driver.findElement(By.linkText("SETTINGS")).click();
		sleep(7000);
		
		
		
		// Select Site to change hours of operation.
		System.out.println("Select Site link.");
		driver.findElement(By.cssSelector("li:nth-of-type(2) > ._7xclgv")).click();
		sleep(7000);
		
		
		// Select Hours of Operation 
		System.out.println("Select Hours of operation  link.");
		driver.findElement(By.cssSelector("div:nth-of-type(2) > .col-12.col-lg-10 .material-icons")).click();
		sleep(7000);
		
		// Enter AM
		System.out.println("Entering AM Hour.");
		driver.findElement(By.cssSelector("input[name='openTime']")).sendKeys(op);
		sleep(3000);
		driver.findElement(By.xpath("/html//div[@id='app-site']/div/div[@class='app-container fixed-drawer']/div[@class='app-main-container']/main[@class='app-main-content-wrapper']/div[@class='app-main-content']/div[@class='container-fluid']/div[@class='row']/div[@class='col-12']/div/div[2]/div[@class='col-12 col-lg-10']//input[@name='openTime']")).sendKeys(min);
		sleep(3000);
		
		
		
		System.out.println("Entering PM Hour.");
		driver.findElement(By.cssSelector("input[name='closeTime']")).sendKeys(cl);
		sleep(3000);
		driver.findElement(By.cssSelector("input[name='closeTime']")).sendKeys(min);
		sleep(3000);

		System.out.println("Select Save Changes.");
		driver.findElement(By.cssSelector("._kjgy2z.btn.btn-block.btn-secondary.btn-sm")).click();
		sleep(7000);


		
		// logout with settings 
		simpleLogout(driver);
		System.out.println("Ending CompanyManagerChangeHoursofOperationTests test.");
		System.out.println("====================================");
	
	

	

	}


}


