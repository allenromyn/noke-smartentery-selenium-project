package com.smartentry.automation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

public class SMSidebarLinkTests extends SMSmartEntryBaseClassEmail {

	@Test
	public void sMSidebarLinkTests() {

	
		//prevents popup
//		ChromeOptions options = preventsPopUp();

		System.out.println("Starting SMSidebarLinkTests test.");
	
		// Login Setup 
		WebDriver driver = loginSetup();
	
		
		
				// Home sidebar
				System.out.println("Select Home..!");
				driver.findElement(By.linkText("HOME")).click();
				sleep(4900);
		
				
				// select Units button
				System.out.println("Selecgt Units..!");
				driver.findElement(By.linkText("UNITS")).click();
				sleep(4900);
		
		
				// select Users button
				System.out.println("Selecgt Users..!");
				driver.findElement(By.linkText("USERS")).click();
				sleep(4900);
		
				
				// select Entries button
				System.out.println("Selecgt Entries..!");
				driver.findElement(By.linkText("ENTRIES")).click();
				sleep(4900);
			
				
				// select Gateways button
				System.out.println("Selecgt Gateways..!");
				driver.findElement(By.linkText("GATEWAYS")).click();
				sleep(4900);
				
				
				// select Activities button
				System.out.println("Selecgt Activity..!");
				driver.findElement(By.linkText("ACTIVITY")).click();
				sleep(4900);
				
					
					
				// select Settings button
				System.out.println("Selecgt Settings..!");
				driver.findElement(By.linkText("SETTINGS")).click();
				sleep(4900);
				
				
				
				// select Help button
				System.out.println("Selecgt Help..!");
				driver.findElement(By.linkText("HELP")).click();
				sleep(4900);
		
				
		// logout method
		logoutMainMethod(driver);

	
		System.out.println("Ending SMSidebarLinkTests test.");
		System.out.println("=======================================");
		// successful

	}

	
}
	


