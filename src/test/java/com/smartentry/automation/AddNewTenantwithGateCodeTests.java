package com.smartentry.automation;



import org.openqa.selenium.By;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;



public class AddNewTenantwithGateCodeTests extends SMSmartEntryBaseClassEmail {

	@Test
	public void addnewtenantwithgatecodeTests() {

		// reuseable variables
		String ac = "123456";
		String un = "3013";
		//String tc = "13";
	
		
	
		System.out.println("Noke home page open..!");
		System.out.println("Start AddNewTenantwithGateCodeTests");
		
		// select plus button
		WebElement add_circle = driver.findElement(By.cssSelector("i#add_circle_outline"));
		add_circle.click();
		sleep(4500);


		// Assign Unit
		WebElement assign_unit = driver.findElement(By.xpath("/html//div[@id='app-site']/div/div[@class='app-container fixed-drawer']//ul[@class='header-notifications list-inline ml-auto']//div[@class='dropdown show']/div[@role='menu']//ul[@class='list-unstyled']/a[@href='#/app/units/assign']//p[@class='_1jqvzzn']"));
		assign_unit.click();
		sleep(4500);
		
		// Assign Unit Form enter
		// first name
		WebElement form_fn = driver.findElement(By.xpath("/html//div[@id='app-site']/div/div[@class='app-container fixed-drawer']/div[@class='app-main-container']/main[@class='app-main-content-wrapper']/div[@class='app-main-content']//input[@name='firstName']"));
		form_fn.sendKeys(fn);
		sleep(1000);
		// last name
		WebElement form_ln = driver.findElement(By.xpath("/html//div[@id='app-site']/div/div[@class='app-container fixed-drawer']/div[@class='app-main-container']/main[@class='app-main-content-wrapper']/div[@class='app-main-content']//input[@name='lastName']"));
		form_ln.sendKeys(ln);
		sleep(1000);
		// phone
		WebElement form_ph = driver.findElement(By.xpath("/html//div[@id='app-site']/div/div[@class='app-container fixed-drawer']/div[@class='app-main-container']/main[@class='app-main-content-wrapper']/div[@class='app-main-content']//div[@class='_1rcdxja container']//input[@name='phone']"));
		form_ph.sendKeys(ph);
		sleep(1000);
		// phone
		WebElement form_ac = driver.findElement(By.xpath("/html//div[@id='app-site']/div/div[@class='app-container fixed-drawer']/div[@class='app-main-container']/main[@class='app-main-content-wrapper']/div[@class='app-main-content']//input[@name='accessCode']"));
		form_ac.sendKeys(ac);
		sleep(3000);

		// enter unit to search for 
		WebElement search_un = driver.findElement(By.xpath("/html//div[@id='app-site']/div/div[@class='app-container fixed-drawer']/div[@class='app-main-container']/main[@class='app-main-content-wrapper']/div[@class='app-main-content']//div[@class='_1rcdxja container']//div[@class='_107ja4p']/div[@class='_e296pg']/input"));
		search_un.sendKeys(un);
		sleep(6500);
		
 
		// select unit to assign from search list 
		WebElement select_un = driver.findElement(By.cssSelector("._6o398z8 > td:nth-child(3)"));
		select_un.click();
		sleep(6500);
		
		// assign unit 
		WebElement assign_un = driver.findElement(By.cssSelector("._rf2xlc.btn.btn-secondary.btn-sm"));
		assign_un.click();
		sleep(9000);
		
		
		
		
	
		System.out.println("End AddNewTenantwithGateCodeTests");
		
		
	
		
				
		
		// successful
		  
	

		
		
		
		
		 
	}

	

	

	
	}


