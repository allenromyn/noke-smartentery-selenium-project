
package com.smartentry.automation;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;




import java.io.IOException;



public class SMSmartEntryBaseClassPhone {
	WebDriver driver1;
	
	// reuseable variables
	String url = "https://dev.ess.nokepro.com/#/signin";
	String user = "qa@noke.com";
	String pw = "password";
	
	// reuseable variables
	String fn = "SiteMfirst";
	String ln = "SiteMlast";
	String em = "allen.romyn@noke.com";
	String li_ph = "8018889999";
	String role_sm = "Site Manager";
	int tc = 8;
	WebDriver driver;
	// reuseable variables
			
	
	
	// @BeforeTest(alwaysRun = true)
	public void setup() throws IOException {

	//	WebDriver driver = loginmethoda(url, user, pw);
		
		System.out.println("Site Manager Home page open..!");
		

		
}
				

	
	
	// logout , stop servers.
//	@AfterTest(alwaysRun = true)
	public void teardown() throws IOException	{
		
			// verification
		// url secure
		System.out.println("Selecting Logout..!");
		sleep(6000);
		WebElement logout = driver1.findElement(By.linkText("LOGOUT"));
		logout.click();
		sleep(6000);
		// Select logout page link WebElement logbutton
		WebElement logbutton = driver1.findElement(By.xpath("/html/body/div/div/div[2]/div[2]/main/div/div/div/div[2]/div/button"));
		logbutton.click();
		driver1.close();
		System.out.println("Logout.");
		// logout visable
		//Logoutmethod(driver);

		// successful

	}


	
	public WebDriver loginSetup() {
		// reuseable variables

		// clear popup
		Map<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("profile.default_content_setting_values.notifications", 2);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", prefs);

		
		// create driver
		System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
		WebDriver driver = new ChromeDriver(options);

		// maximize window
		driver.manage().window().maximize();
		// open test page
		driver.get(url);
		System.out.println("Noke login page open..!");

		// slow window
		sleep(3500);
//		System.out.println("Change login option..!");
//		// change login optionb
//		WebElement changeloginoption = driver.findElement(By.xpath("/html//div[@id='app-site']/div//form/a[@class='_pj8a4d']"));
//		changeloginoption.click();
		// enter username
		WebElement loginphone = driver.findElement(By.cssSelector("._1g84vr9.form-control"));
		loginphone.sendKeys(li_ph);
		System.out.println("Login with : " + li_ph);
		// enter pw
		WebElement password = driver.findElement(By.cssSelector("._19zwubya.form-control"));
		password.sendKeys(pw);
		// select login arrow
		WebElement loginarrow = driver.findElement(By.xpath("/html//div[@id='app-site']/div//form//button[@type='submit']/i[@class='material-icons']"));
		loginarrow.click();
		// slow window
		System.out.println("Login..!");
		sleep(9000);
		return driver;
	}


	
	
	
	
	
	
	
	
	public void simpleLogout(WebDriver driver) {
		System.out.println("Selecgt Logout..!");
		driver.findElement(By.linkText("LOGOUT")).click();
		sleep(4900);
		driver.findElement(By.xpath("/html//div[@id='app-site']/div/div[@class='app-container fixed-drawer']/div[@class='app-main-container']//div[@class='_uwvy5kr']/div[2]/div[@class='col-12']/button[@type='button']")).click();
		sleep(4900);
		driver.close();
		System.out.println("Logout.");
		System.out.println("Quit driver..!");
		System.out.println("===========================================");
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
		
	
	public WebDriver loginmethoda(String url, String user, String pw) {
		
		// clear popup
		Map<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("profile.default_content_setting_values.notifications", 2);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", prefs);

		System.out.println("Starting companyManagerSettingsTest test.");
		// create driver
		System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
		WebDriver driver = new ChromeDriver(options);

		// maximize window
		driver.manage().window().maximize();
		// open test page
		driver.get(url);
		System.out.println("Noke login page open..!");

		// slow window
		sleep(3500);
		System.out.println("Change login option..!");
		// change login optionb
		WebElement changeloginoption = driver.findElement(By.xpath("/html//div[@id='app-site']/div//form/a[@class='_pj8a4d']"));
		changeloginoption.click();
		// enter username
		WebElement username = driver.findElement(By.cssSelector("._1g84vr9.form-control"));
		username.sendKeys(user);
		System.out.println("Login with : " + user);
		// enter pw
		WebElement password = driver.findElement(By.cssSelector("._19zwubya.form-control"));
		password.sendKeys(pw);
		// select login arrow
		WebElement loginarrow = driver.findElement(By.xpath("/html//div[@id='app-site']/div//form//button[@type='submit']/i[@class='material-icons']"));
		loginarrow.click();
		// slow window
		System.out.println("Login..!");
		sleep(9000);
		return driver;
		
	}
	

	public  void sleep(long m) {
		try {
			Thread.sleep(m);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
