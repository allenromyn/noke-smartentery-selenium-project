package com.smartentry.automation;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

public class SMSettingsLinksTests {

	@Test
	public void sMSettingsTests() {

		// reuseable variables
		String url = "https://dev.ess.nokepro.com/#/signin";
		String user = "qa@noke.com";
		String pw = "password";

		// clear popup
		Map<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("profile.default_content_setting_values.notifications", 2);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", prefs);

		System.out.println("Starting SMSettingsTests test.");
		// create driver
		System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
		WebDriver driver = new ChromeDriver(options);

		// maximize window
		driver.manage().window().maximize();
		// open test page
		driver.get(url);
		System.out.println("Noke login page open..!");

		// slow window
		sleep(3500);
		System.out.println("Change login option..!");
		// change login optionb
		WebElement changeloginoption = driver.findElement(By.xpath("/html//div[@id='app-site']/div//form/a[@class='_pj8a4d']"));
		changeloginoption.click();
		// enter username
		WebElement username = driver.findElement(By.cssSelector("._1g84vr9.form-control"));
		username.sendKeys(user);
		System.out.println("Login with : " + user);
		// enter pw
		WebElement password = driver.findElement(By.cssSelector("._19zwubya.form-control"));
		password.sendKeys(pw);
		// select login arrow
		WebElement loginarrow = driver.findElement(By.xpath("/html//div[@id='app-site']/div//form//button[@type='submit']/i[@class='material-icons']"));
		loginarrow.click();
		// slow window
		System.out.println("Login..!");
		sleep(9000);


		// select settings button
		System.out.println("Selecgt Settings..!");
		WebElement sidebarsettings = driver.findElement(By.linkText("SETTINGS"));
		sidebarsettings.click();
		sleep(4900);

		// Change theme for color effect
		System.out.println("Change theme..!");
		WebElement linktheme = driver.findElement(By.cssSelector("._1bvrdsje > ._7xclgv")); // Theme
		linktheme.click();
		sleep(3500);
		// Change color mode
		WebElement thememode = driver.findElement(By.cssSelector("select[name='select']"));
		thememode.click();
		sleep(1000);
		// change current mode to dark mode
		WebElement modedark = driver.findElement(By.cssSelector("select[name='select']"));
		modedark.sendKeys("Dark Mode");
		modedark.click();
		sleep(3500);

		// select site settings
		WebElement linksite = driver.findElement(By.cssSelector("li:nth-of-type(2) > ._7xclgv")); // site
		linksite.click();
		sleep(3500);
		
		sidebarsettings.click();
		 
		// select site branding
		// Branding
		WebElement linksettingsbranding = driver.findElement(By.cssSelector("li:nth-of-type(3) > ._7xclgv")); // Branding
		linksettingsbranding.click();
		sleep(3500);

		// select site Zone
		// Zone
		WebElement linksettingszone = driver.findElement(By.cssSelector("li:nth-of-type(4) > ._7xclgv")); // Zoaning
		linksettingszone.click();
		sleep(6000);

		// select site Roles
		// Roles
		WebElement linksettingsroles = driver.findElement(By.cssSelector("li:nth-of-type(5) > ._7xclgv")); // Roles
		linksettingsroles.click();
		sleep(6000);

		// select site Security
		// Security
		WebElement linksettingssecurity = driver.findElement(By.cssSelector("li:nth-of-type(6) > ._7xclgv")); // Roles
		linksettingssecurity.click();
		sleep(3500);

		// Home sidebar
		System.out.println("Select Home..!");
		WebElement linkhome = driver.findElement(By.linkText("HOME"));
		linkhome.click();
		sleep(4500);

		// verification
		// url secure
		System.out.println("Selecting Logout..!");
		sleep(6000);
		WebElement logout = driver.findElement(By.linkText("LOGOUT"));
		logout.click();
		sleep(6000);
		// Select logout page link WebElement logbutton
		WebElement logbutton = driver.findElement(By.xpath("/html//div[@id='app-site']/div/div[@class='app-container fixed-drawer']/div[@class='app-main-container']//div[@class='_1cfzzm9v']/div[2]/div[@class='col-12']/button[@type='button']"));
		logbutton.click();
		driver.close();
		System.out.println("Logout.");
		// logout visable
		//Logoutmethod(driver);

		// successful

	}


	private void sleep(long m) {
		try {
			Thread.sleep(m);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
