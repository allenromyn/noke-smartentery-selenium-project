package com.smartentry.automation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class SMSettingsChangeModeTests extends SMSmartEntryBaseClassEmail{

	@Test
	public void sMSettingsChangeModeTests() {

		
		System.out.println("Starting SMSettingsChangeModeTests Test.");
		
		// Login Setup 
		WebDriver driver = loginSetup();


			// select settings button
			System.out.println("Selecgt Settings..!");
			WebElement sidebarsettings = driver.findElement(By.linkText("SETTINGS"));
			sidebarsettings.click();
			sleep(4900);
	
			// Change theme for color effect
			System.out.println("Change theme..!");
			WebElement linktheme = driver.findElement(By.cssSelector("._1bvrdsje > ._7xclgv")); // Theme
			linktheme.click();
			sleep(3500);
			// Change color mode
			WebElement thememode = driver.findElement(By.cssSelector("select[name='select']"));
			thememode.click();
			sleep(1000);
			// change current mode to dark mode
			WebElement modedark = driver.findElement(By.cssSelector("select[name='select']"));
			modedark.sendKeys("Dark Mode");
			modedark.click();
			sleep(3500);
	
			// select site settings
			WebElement linksite = driver.findElement(By.cssSelector("li:nth-of-type(2) > ._7xclgv")); // site
			linksite.click();
			sleep(3500);
			
			sidebarsettings.click();
			 
			// select site branding
			// Branding
			WebElement linksettingsbranding = driver.findElement(By.cssSelector("li:nth-of-type(3) > ._7xclgv")); // Branding
			linksettingsbranding.click();
			sleep(3500);
	
			// select site Zone
			// Zone
			WebElement linksettingszone = driver.findElement(By.cssSelector("li:nth-of-type(4) > ._7xclgv")); // Zoaning
			linksettingszone.click();
			sleep(6000);
	
			// select site Roles
			// Roles
			WebElement linksettingsroles = driver.findElement(By.cssSelector("li:nth-of-type(5) > ._7xclgv")); // Roles
			linksettingsroles.click();
			sleep(6000);
	
			// select site Security
			// Security
			WebElement linksettingssecurity = driver.findElement(By.cssSelector("li:nth-of-type(6) > ._7xclgv")); // Roles
			linksettingssecurity.click();
			sleep(3500);
	
			// Home sidebar
			System.out.println("Select Home..!");
			WebElement linkhome = driver.findElement(By.linkText("HOME"));
			linkhome.click();
			sleep(4500);

		// Simple logout
		simpleLogout(driver);
		System.out.println("Ending SMSettingsChangeModeTests Test.");
		System.out.println("======================================");

	}

}


