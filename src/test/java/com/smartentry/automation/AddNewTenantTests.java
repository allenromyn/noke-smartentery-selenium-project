package com.smartentry.automation;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

public class AddNewTenantTests {
	
	
	@Test
	public void addnewtenantTests() {
	{

	// reuseable variables
	String url = "https://dev.ess.nokepro.com/#/signin";
	String user = "qa@noke.com";
	String pw = "password";
	String fn = "NewFirst";
	String ln = "NewLast";
	String em = "allen.romyn@noke.com";
	String ph = "4038887777";
	String role_sm = "Tenant";
	
	// clear popup
	Map<String, Object> prefs = new HashMap<String, Object>();
	prefs.put("profile.default_content_setting_values.notifications", 2);
	ChromeOptions options = new ChromeOptions();
	options.setExperimentalOption("prefs", prefs);

	System.out.println("Starting Add New tenant.");
	// create driver
	System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
	WebDriver driver = new ChromeDriver(options);

	// maximize window
	driver.manage().window().maximize();
	// open test page
	driver.get(url);
	System.out.println("Noke home page open..!");

	// login main 
	Loginmain(user, pw, driver);
	// Change store
	ChangeStore(driver);

	// select plus button
	WebElement add_circle = driver.findElement(By.cssSelector("i#add_circle_outline"));
	add_circle.click();
	sleep(4500);


	// Add new user 
	WebElement addnewuser = driver.findElement(By.xpath("/html//div[@id='app-site']/div/div[@class='app-container fixed-drawer']//ul[@class='header-notifications list-inline ml-auto']//div[@class='dropdown show']/div[@role='menu']//ul[@class='list-unstyled']/a[@href='#/app/users/create']//p[@class='_1jqvzzn']"));
	addnewuser.click();
	sleep(5000);
	
	// Add new user form 
	// first name
	WebElement form_fn = driver.findElement(By.xpath("/html//div[@id='app-site']/div/div[@class='app-container fixed-drawer']/div[@class='app-main-container']/main[@class='app-main-content-wrapper']/div[@class='app-main-content']//input[@name='firstName']"));
	form_fn.sendKeys(fn);
	sleep(1000);
	// last name
	WebElement form_ln = driver.findElement(By.xpath("/html//div[@id='app-site']/div/div[@class='app-container fixed-drawer']/div[@class='app-main-container']/main[@class='app-main-content-wrapper']/div[@class='app-main-content']//input[@name='lastName']"));
	form_ln.sendKeys(ln);
	sleep(1000);
	// phone
	WebElement form_email = driver.findElement(By.xpath("/html//div[@id='app-site']/div/div[@class='app-container fixed-drawer']/div[@class='app-main-container']/main[@class='app-main-content-wrapper']/div[@class='app-main-content']//input[@name='email']"));
	form_email.sendKeys(em);
	sleep(1000);

	// phone
	WebElement form_ph = driver.findElement(By.xpath("/html//div[@id='app-site']/div/div[@class='app-container fixed-drawer']/div[@class='app-main-container']/main[@class='app-main-content-wrapper']/div[@class='app-main-content']//div[@class='_1rcdxja container']//input[@name='phone']"));
	form_ph.sendKeys(ph);
	sleep(1000);
	// add new role
	WebElement createrole = driver.findElement(By.xpath("/html//div[@id='app-site']/div/div[@class='app-container fixed-drawer']/div[@class='app-main-container']/main[@class='app-main-content-wrapper']/div[@class='app-main-content']//select[@name='type']"));
	createrole.sendKeys(role_sm);
	sleep(3000);

	// Submit form
	WebElement submit_form_adduser = driver.findElement(By.xpath("/html//div[@id='app-site']/div/div[@class='app-container fixed-drawer']/div[@class='app-main-container']//div[@class='col-12 col-md-4']/button[@type='button']"));
	submit_form_adduser.click();
	sleep(3000);

	
	
	
	 // logout visable
	 Logoutmethod(driver);
	  


	 driver.close();
     driver.quit();
		
	
	
	
	
	
	
	
	}

	
	
			
	
	// successful
	  


	
	
	
	
	 
}

private void ChangeStore(WebDriver driver) {
	// Change store to Smart Storage - can change to anyone
	System.out.println("Change Store..!");
	WebElement changestorelink = driver.findElement(By.cssSelector("._yp2l2r"));
	changestorelink.click(); 
	sleep(4000);
	WebElement changestore = driver.findElement(By.cssSelector("._1bw3whk3 > ._ap8buj"));
	changestore.click();
	sleep(9000);
}

private void Loginmain(String user, String pw, WebDriver driver) {
	// change login optionb
	WebElement changeloginoption = driver.findElement(By.xpath("/html//div[@id='app-site']/div//form/a[@class='_pj8a4d']"));
	changeloginoption.click();
	sleep(2500);
	// enter username
	WebElement username = driver.findElement(By.cssSelector("._1g84vr9.form-control"));
	username.sendKeys(user);
	sleep(3000);
	// enter pw
	WebElement password = driver.findElement(By.cssSelector("._19zwubya.form-control"));
	password.sendKeys(pw);
	sleep(3000);
	// select login arrow
	WebElement loginarrow = driver.findElement(By.xpath("/html//div[@id='app-site']/div//form//button[@type='submit']/i[@class='material-icons']"));
	loginarrow.click();
	// slow window
	System.out.println("Login..!");
	sleep(9000);
}

private void Logoutmethod(WebDriver driver) {
	WebElement logout = driver.findElement(By.linkText("LOGOUT"));
	  logout.click(); 
	  sleep(5000); 
	  // Select logout page link WebElement logbutton
	  WebElement logbutton =  driver.findElement(By.cssSelector("._1okleyb.btn.btn-lg.btn-secondary"));
	  logbutton.click(); 
	  driver.close();
	  System.out.println("Logout.");
}

private void sleep(long m) {
	try {
		Thread.sleep(m);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

}

