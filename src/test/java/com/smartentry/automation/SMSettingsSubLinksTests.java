package com.smartentry.automation;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class SMSettingsSubLinksTests extends SMSmartEntryBaseClassEmail   {

	@Test
	public void companyManagerSettingsTest() {

 		System.out.println("Starting SMSettingsSubLinksTests test.");

		// Login Setup 
		WebDriver driver = loginSetup();
	
			// select settings button
			System.out.println("Selecgt Settings..!");
			WebElement sidebarsettings = driver.findElement(By.linkText("SETTINGS"));
			sidebarsettings.click();
			sleep(4900);
	
			// Change theme for color effect
			System.out.println("Change theme..!");
			driver.findElement(By.cssSelector("._1bvrdsje > ._7xclgv")).click();
			sleep(3500);
			// Change color mode
			driver.findElement(By.cssSelector("select[name='select']")).click();
			sleep(1000);
			// change current mode to dark mode
			WebElement modedark = driver.findElement(By.cssSelector("select[name='select']"));
			modedark.sendKeys("Dark Mode");
			modedark.click();
			sleep(3500);
	
			// select site settings
			driver.findElement(By.cssSelector("li:nth-of-type(2) > ._7xclgv")).click();
			sleep(3500);
			
			sidebarsettings.click();
			 
			// select site branding
			// Branding
			driver.findElement(By.cssSelector("li:nth-of-type(3) > ._7xclgv")).click();
			sleep(3500);
	
			// select site Zone
			// Zone
			driver.findElement(By.cssSelector("li:nth-of-type(4) > ._7xclgv")).click();
			sleep(6000);
	
			// select site Roles
			// Roles
			driver.findElement(By.cssSelector("li:nth-of-type(5) > ._7xclgv")).click();
			sleep(6000);
	
			// select site Security
			// Security
			driver.findElement(By.cssSelector("li:nth-of-type(6) > ._7xclgv")).click();
			sleep(3500);
	
			// Home sidebar
			System.out.println("Select Home..!");
			driver.findElement(By.linkText("HOME")).click();
			sleep(4500);

		
		
		// logout with settings 
		logoutSettings(driver);
		System.out.println("Ending SMSettingsSubLinksTests test.");
		System.out.println("====================================");
		
		
		
	}

	
}

	


