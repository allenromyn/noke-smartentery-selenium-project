package com.smartentry.automation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class SMHelpLinksTests extends SMSmartEntryBaseClassEmail {
	String tn = "Billy Blonco";
	String tn_ph = "8018889999";
	String dt = "Detail entered with automation..! Ignore message..! There is no issue..!";

	
	
	
	
	@Test
	public void sMHelpLinksTests() {

	
		System.out.println("Starting SMHelpLinksTests test.");

		// Login Setup 
		WebDriver driver = loginSetup();
	
		
	
			// select Help from sidebar
			driver.findElement(By.linkText("HELP")).click();
			System.out.println("Select Help..!");
			sleep(4500);
	
			
			// select Help Troubleshoot
			WebElement helptroubleshoot = driver.findElement(By.cssSelector("li:nth-of-type(2) > ._7xclgv"));
			helptroubleshoot.click();
			sleep(4500);
			System.out.println("Selecting Troubleshoot..!");
	
			// select help submit ticket.
			WebElement helpsubmitticket = driver.findElement(By.cssSelector("li:nth-of-type(3) > ._7xclgv"));
			helpsubmitticket.click();
			sleep(3000);
			System.out.println("Select Submit Tickett..!");
			
				driver.findElement(By.cssSelector("input[name='tenantName']")).sendKeys(tn);
				sleep(3000);
				System.out.println("Enter tenants name..!");
	
				driver.findElement(By.cssSelector("textarea[name='details']")).sendKeys(dt);
				sleep(3000);
				System.out.println("Enter ticket details..!");
			
//				driver.findElement(By.cssSelector("._14mjkej.btn.btn-secondary.btn-sm")).click();
//				sleep(7000);
//				System.out.println("Submit Tickett..!");
			
				// when submit ticket , a email send window pops up

			// select help submit ticket.
			driver.findElement(By.cssSelector("._r2kksae > ._7xclgv")).click();
			System.out.println("Select Contact Us..!");
			sleep(7000);
			
			// logout 
				
				
				
		// logout with settings 
		simpleLogout(driver);
		System.out.println("Ending SMHelpLinksTests test.");
		System.out.println("====================================");


	}

}


