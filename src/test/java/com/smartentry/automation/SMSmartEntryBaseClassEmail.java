
package com.smartentry.automation;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.IOException;



public class SMSmartEntryBaseClassEmail {

	// reuseable variables
	String url = "https://dev.ess.nokepro.com/#/signin";
	String li_em = "qa@noke.com";
	String user = "qa@noke.com";
	String pw = "password";
	
	// reuseable variables
	String fn = "NewSiteMfirst";
	String ln = "NewSiteMlast";
	String em = "allen.romyn@noke.com";
	String ph = "8018889898";
	String role_sm = "Site Manager";
	int tc = 8;
	WebDriver driver;
	// reuseable variables
	
		
	
	
	// @BeforeTest(alwaysRun = true)
	public void setup() throws Exception {

		//prevents popup
		ChromeOptions options = preventsPopUp();
		System.out.println("Prevent Popup..!");

		//setup driver - maximize 
		WebDriver driver = setupDriver(options);
		
		//change to email login
		changeToEmail(driver);
		
		// Login with email
		loginWithEmail(driver);
		sleep(5000);
		
}



	
	
	
	
	
	
	
	
	
	
	public WebDriver loginSetup() {
		// reuseable variables

		// clear popup
		Map<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("profile.default_content_setting_values.notifications", 2);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", prefs);

		
		// create driver
		System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
		WebDriver driver = new ChromeDriver(options);

		// maximize window
		driver.manage().window().maximize();
		// open test page
		driver.get(url);
		System.out.println("Noke login page open..!");

		// slow window
		sleep(3500);
		System.out.println("Change login option..!");
		// change login optionb
		WebElement changeloginoption = driver.findElement(By.xpath("/html//div[@id='app-site']/div//form/a[@class='_pj8a4d']"));
		changeloginoption.click();
		// enter username
		WebElement username = driver.findElement(By.cssSelector("._1g84vr9.form-control"));
		username.sendKeys(user);
		System.out.println("Login with : " + user);
		// enter pw
		WebElement password = driver.findElement(By.cssSelector("._19zwubya.form-control"));
		password.sendKeys(pw);
		// select login arrow
		WebElement loginarrow = driver.findElement(By.xpath("/html//div[@id='app-site']/div//form//button[@type='submit']/i[@class='material-icons']"));
		loginarrow.click();
		// slow window
		System.out.println("Login..!");
		sleep(9000);
		return driver;
	}

	
	
	
	
	
	
	
	
	


	public void loginWithEmail(WebDriver driver) {
		// enter username / phone
		WebElement username = driver.findElement(By.cssSelector("._1g84vr9.form-control"));
		username.sendKeys(li_em);
		System.out.println("Login with: " + li_em + "..!");
		sleep(3000);
		
		// enter pw
		WebElement password = driver.findElement(By.cssSelector("._19zwubya.form-control"));
		password.sendKeys(pw);
		System.out.println("Enter Password..!");
		// select login arrow
		WebElement loginarrow = driver.findElement(By.xpath("/html//div[@id='app-site']/div//form//button[@type='submit']/i[@class='material-icons']"));
		loginarrow.click();
		// slow window
		System.out.println("Login..!");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println("Site Manager Home page open..!");
		sleep(9000);
	}





	public void changeToEmail(WebDriver driver) {
		// slow window
		sleep(5000);
		// change login optionb from phone to email 
		WebElement changeloginoption = driver.findElement(By.xpath("/html//div[@id='app-site']/div//form/a[@class='_pj8a4d']"));
		changeloginoption.click();
	}





	public WebDriver setupDriver(ChromeOptions options) {
		// create driver
		System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
		WebDriver driver = new ChromeDriver(options);
		System.out.println("Web Driver being created.");
		// slow window
		sleep(3000);	
		
		// maximize window
		System.out.println("Noke home maximize page open..!");
		driver.manage().window().maximize();
		// open test page
		driver.get(url);
		return driver;
	}





	public ChromeOptions preventsPopUp() {
		System.out.println("Prevents page popup.");
		// clear popup on main page - warning 
		Map<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("profile.default_content_setting_values.notifications", 2);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", prefs);
		return options;
	}
				
	

	
	
	// logout , stop servers.
//	@AfterTest(alwaysRun = true)
	public void teardown() throws IOException{
		
				

		// verification
		// url secure
		
	
	
		
		
		System.out.println("Logout.");
		System.out.println("Quit driver..!");
		System.out.println("===========================================");
	}


	
	
	
	public void logoutSettings(WebDriver driver) {
		System.out.println("Selecting Logout..!");
		sleep(6000);
		WebElement logout = driver.findElement(By.linkText("LOGOUT"));
		logout.click();
		sleep(6000);
		// Select logout page link WebElement logbutton
		WebElement logbutton = driver.findElement(By.xpath("/html//div[@id='app-site']/div/div[@class='app-container fixed-drawer']/div[@class='app-main-container']//div[@class='_1cfzzm9v']/div[2]/div[@class='col-12']/button[@type='button']"));
		logbutton.click();
		driver.close();
		System.out.println("Logout.");
		System.out.println("Quit driver..!");
		System.out.println("===========================================");
	
	}
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	public void logoutMainMethod(WebDriver driver) {
		System.out.println("Selecgt Logout..!");
		driver.findElement(By.linkText("LOGOUT")).click();
		sleep(4900);
		// Select logout page link WebElement logbutton
		WebElement logbutton = driver.findElement(By.xpath("/html//div[@id='app-site']/div/div[@class='app-container fixed-drawer']/div[@class='app-main-container']//div[@class='_uwvy5kr']/div[2]/div[@class='col-12']/button[@type='button']"));
		logbutton.click();
		driver.close();
		System.out.println("Logout.");
		System.out.println("Quit driver..!");
		System.out.println("===========================================");
	}

	
	
	
	
	public void simpleLogout(WebDriver driver) {
		System.out.println("Selecgt Logout..!");
		driver.findElement(By.linkText("LOGOUT")).click();
		sleep(4900);
		driver.findElement(By.xpath("/html//div[@id='app-site']/div/div[@class='app-container fixed-drawer']/div[@class='app-main-container']//div[@class='_uwvy5kr']/div[2]/div[@class='col-12']/button[@type='button']")).click();
		sleep(4900);
		driver.close();
		System.out.println("Logout.");
		System.out.println("Quit driver..!");
		System.out.println("===========================================");
	}

	
	
	
	
	
	
	
	
	
	
	
	
	


	public void logoutMethod() {
		System.out.println("Selecting Logout..!");
		sleep(6000);
		WebElement logout = driver.findElement(By.xpath("//a[@id='pages.logout.TYPE']//span[@class='_soxzkj']"));
		logout.click();
		sleep(6000);
		// Select logout page link WebElement logbutton
		WebElement logbutton = driver.findElement(By.xpath("/html//div[@id='app-site']/div/div[@class='app-container fixed-drawer']/div[@class='app-main-container']//div[@class='_1cfzzm9v']/div[2]/div[@class='col-12']/button[@type='button']"));
		logbutton.click();
		driver.close();
		System.out.println("Logout.");
	}
		
	
	public  void sleep(long m) {
		try {
			Thread.sleep(m);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}



//private void ChangeStore(WebDriver driver) {
//	// Change store to Smart Storage - can change to anyone
//	System.out.println("Change Store..!");
//	WebElement changestorelink = driver.findElement(By.cssSelector("._yp2l2r"));
//	changestorelink.click(); 
//	sleep(4000);
//	WebElement changestore = driver.findElement(By.cssSelector("._1bw3whk3 > ._ap8buj"));
//	changestore.click();
//	sleep(9000);
//}

//private void Loginmain(String user, String pw, WebDriver driver) {
//	// change login optionb
//	WebElement changeloginoption = driver.findElement(By.xpath("/html//div[@id='app-site']/div//form/a[@class='_pj8a4d']"));
//	changeloginoption.click();
//	sleep(2500);
//	// enter username
//	WebElement username = driver.findElement(By.cssSelector("._1g84vr9.form-control"));
//	username.sendKeys(user);
//	sleep(3000);
//	// enter pw
//	WebElement password = driver.findElement(By.cssSelector("._19zwubya.form-control"));
//	password.sendKeys(pw);
//	sleep(3000);
//	// select login arrow
//	WebElement loginarrow = driver.findElement(By.xpath("/html//div[@id='app-site']/div//form//button[@type='submit']/i[@class='material-icons']"));
//	loginarrow.click();
//	// slow window
//	System.out.println("Login..!");
//	sleep(9000);
//}

//private void Logoutmethod(WebDriver driver) {
//	WebElement logout = driver.findElement(By.linkText("LOGOUT"));
//	  logout.click(); 
//	  sleep(5000); 
//	  // Select logout page link WebElement logbutton
//	  WebElement logbutton =  driver.findElement(By.cssSelector("._1okleyb.btn.btn-lg.btn-secondary"));
//	  logbutton.click(); 
//	  driver.close();
//	  System.out.println("Logout...!");
//	}
}